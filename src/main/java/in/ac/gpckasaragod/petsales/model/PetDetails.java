/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package in.ac.gpckasaragod.petsales.model.ui.data;

/**
 *
 * @author student
 */
    public    class PetDetails {

    public PetDetails(Integer id, String petname,  Double price) {
        this.id = id;
        this.petname = petname;
       
        
        this.price = price;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPetname() {
     return petname;
        
    }

    public void setPetname(String petname) {
        this.petname = petname;
    }

    

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }
       private Integer id;
       private String petname;
       private Integer petid;
       private Double price;
}
