/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package in.ac.gpckasaragod.petsales.service;

import in.ac.gpckasaragod.petsales.model.ui.data.PetDetails;
import in.ac.gpckasaragod.petsales.ui.PetDetailForm;
import java.util.List;

/**
 *
 * @author student
 */
public interface PetDetailsService{
    public String savePetDetails(Integer Id,Integer PetId,String PetName,Double Price);
    public PetDetailForm readPetDetails(Integer Id);
    public List <PetDetailForm> getAllPetDetail();
    
    public String updatePetDetails(Integer Id,Integer PetId,String PetName,Double Price);

    public PetDetailForm readPetDetailForm(Integer id);

    public String deletePetDetails(Integer id);

    public List<PetDetails> getAllPetDetails();
   
    
    

   }

