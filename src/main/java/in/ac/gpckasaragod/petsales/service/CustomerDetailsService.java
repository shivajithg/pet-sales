/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package in.ac.gpckasaragod.petsales.service;

import in.ac.gpckasaragod.petsales.ui.CustomerDetailForm;
import java.util.List;

/**
 *
 * @author student
 */
public interface CustomerDetailsService {
  public String saveCustomerDetails(Integer PetId,String PetName,Double Price);
    public CustomerDetailForm readCustomerDetails(Integer PetId);
    public List <CustomerDetailForm> getAllCustomerDetail();
    public String updateCustomerDetails(CustomerDetailForm customerdetails);
    public String updateCustomerDetails(Integer PetId,String PetName,Double Price);
      
}
