/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package in.ac.gpckasaragod.petsales.service.impl;

import in.ac.gpckasaragod.petsales.service.CustomerDetailsService;
import in.ac.gpckasaragod.petsales.service.PetDetailsService;
import in.ac.gpckasaragod.petsales.ui.CustomerDetailForm;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author student
 */
public class CustomerDetailsServiceImpl  extends ConnectionServiceImpl  implements CustomerDetailsService{
   public String saveCustomerDetails(Integer id,String customerName, Integer petId,Double price){
 try{  Connection connection = getConnection();
   Statement statement = connection.createStatement();
      String query ="INSERT INTO CUSTOMER_DETAILS(ID,CUSTOMER_NAME,PET_NAME,PRICE)Values";
      System.err.println("Query:"+query);
      int status = statement.executeUpdate(query);
      if(status !=1){
            return "Save failed";
      }else{
          return "Saved succesfully";
      }
      
      } catch (SQLException ex) {
           Logger.getLogger(CustomerDetailsServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
       }
      
      return "save failed";
   }
  
   @Override
   public CustomerDetailForm readCustomerDetails(Integer id) {
       CustomerDetailForm customerDetails = null;
       try{
            Connection connection= getConnection();
            Statement statement = connection.createStatement();
            String query = "SELECT * FROM  CUSTOMER_DETAILS WHERE ID="+id;
            ResultSet resultset = statement.executeQuery(query);
             
            while (resultset.next()){
                Integer Id = resultset.getInt("ID");
                String CustomerName = resultset.getString("CUSTOMER_NAME");
                Integer PetName =resultset.getInt("PET_NAME");
                Double Price= resultset.getDouble("PRICE");
            }
                
                return customerDetails;
                        }catch (SQLException ex) {
           Logger.getLogger(PetDetailsServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
       }
                return customerDetails;
   }
   public List<CustomerDetailForm>getAllCustomerDetails(){
       List<CustomerDetailForm>CustomerDetails =new ArrayList<>();
        try{
             Connection connection= getConnection();
             Statement statement = connection.createStatement();
            String query = "SELECT * FROM CUSTOMER_DETAILS WHERE ID=";
            ResultSet resultset = statement.executeQuery(query);
             
             while (resultset.next()){
                Integer Id = resultset.getInt("ID");
                String CustomerName = resultset.getString("CUSTOMER_NAME");
                Integer PetName =resultset.getInt("PET_NAME");
                Double Price= resultset.getDouble("PRICE");
            
            }
            
        }catch (SQLException ex) {
           Logger.getLogger(PetDetailsServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
       }
                return CustomerDetails;
        

   }

    @Override
    public String saveCustomerDetails(Integer PetId, String PetName, Double Price) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public List<CustomerDetailForm> getAllCustomerDetail() {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public String updateCustomerDetails(CustomerDetailForm customerdetails) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public String updateCustomerDetails(Integer PetId, String PetName, Double Price) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }
}











