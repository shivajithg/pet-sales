/* 
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Other/SQLTemplate.sql to edit this template
 */
/**
 * Author:  student
 * Created: 08-Jun-2022
*/

DROP DATABASE PET_SALES;
CREATE DATABASE IF NOT EXISTS PET_SALES;
USE PET_SALES;
CREATE TABLE IF NOT EXISTS PET_DETAILS(ID INT PRIMARY KEY AUTO_INCREMENT,PET_NAME VARCHAR(20) NOT NULL,PET_PRICE DECIMAL(8,2));
DESC PET_DETAILS;
CREATE TABLE  IF NOT EXISTS CUSTOMER_DETAILS(ID INT  PRIMARY KEY AUTO_INCREMENT,CUSTOMER_NAME VARCHAR(20) NOT NULL,PHONE_NUM VARCHAR(12) NOT NULL,
PET_PRICE DECIMAL(8,2),PET_ID INT,FOREIGN KEY(PET_ID) REFERENCES PET_DETAILS(ID));
DESC CUSTOMER_DETAILS;

 





